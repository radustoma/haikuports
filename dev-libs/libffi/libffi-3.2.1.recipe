SUMMARY="A portable, high level programming interface."
DESCRIPTION="
A portable, high level programming interface to various calling conventions.
"
HOMEPAGE="http://sourceware.org/libffi" 
LICENSE="MIT"
REVISION="2"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

SRC_URI="ftp://sourceware.org/pub/libffi/libffi-$portVersion.tar.gz"
CHECKSUM_SHA256="d06ebb8e1d9a22d19e38d63fdb83954253f39bedc5d46232a05645685722ca37"
COPYRIGHT="1996-2013 Anthony Green, Red Hat, Inc and others."
PATCHES="libffi-$portVersion.patchset"

PROVIDES="
	libffi$secondaryArchSuffix = $portVersion compat >= 3
	lib:libffi$secondaryArchSuffix = 6.0.4 compat >= 6
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

BUILD_REQUIRES="
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:make
	cmd:awk
	"

BUILD()
{
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	mkdir -p $includeDir
	mv -f $libDir/libffi-$portVersion/include/*.h* $includeDir
	rm -rf $libDir/libffi-$portVersion
	
	prepareInstalledDevelLibs libffi
	fixPkgconfig
	
	# devel package
	packageEntries devel \
		$developDir \
		$documentationDir
}

PROVIDES_devel="
	libffi${secondaryArchSuffix}_devel = $portVersion
	devel:libffi$secondaryArchSuffix = 6.0.4 compat >= 6
	"

REQUIRES_devel="
	libffi$secondaryArchSuffix == $portVersion base
	"
