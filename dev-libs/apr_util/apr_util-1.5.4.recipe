SUMMARY="Apache Portable Runtime Utility Library"
DESCRIPTION="
The mission of the Apache Portable Runtime (APR) project is to create and \
maintain software libraries that provide a predictable and consistent \
interface to underlying platform-specific implementations. The primary goal is \
to provide an API to which software developers may code and be assured of \
predictable if not identical behaviour regardless of the platform on which \
their software is built, relieving them of the need to code special-case \
conditions to work around or take advantage of platform-specific deficiencies \
or features.
"
HOMEPAGE="http://apr.apache.org/"
SRC_URI="http://archive.apache.org/dist/apr/apr-util-$portVersion.tar.gz"
CHECKSUM_SHA256="976a12a59bc286d634a21d7be0841cc74289ea9077aa1af46be19d1a6e844c19"
LICENSE="Apache v2"
COPYRIGHT="2011 The Apache Software Foundation."
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"

PATCHES="apr_util-1.5.3.patch"

PROVIDES="
	apr_util = $portVersion compat >= 1
	lib:libaprutil_1 = 0.5.4 compat >= 0
	"
REQUIRES="
	haiku >= $haikuVersion
	lib:libapr_1
	lib:libexpat
	lib:libiconv
	"
BUILD_REQUIRES="
	devel:libapr_1 >= 0.5.0
	devel:libexpat
	devel:libiconv
	"
BUILD_PREREQUIRES="
	haiku_devel >= $haikuVersion
	cmd:aclocal
	cmd:autoconf
	cmd:autoheader
	cmd:gcc
	cmd:ld
	cmd:libtoolize
	cmd:make
	"

SOURCE_DIR="apr-util-$portVersion"

PATCH()
{
	echo 'AM_INIT_AUTOMAKE' >> xml/expat/configure.in
}

BUILD()
{
	aprInstallDir=$portPackageLinksDir/lib~libapr_1
	expatInstallDir=$portPackageLinksDir/lib~libexpat

	rm -rf aclocal.m4
	mkdir -p m4
	libtoolize -fci
	aclocal --install -I m4
	autoconf -f

	cd xml/expat
	mkdir -p m4
	libtoolize -fci
	aclocal --install -I m4
	autoconf --force
	autoheader
	touch libtool.m4
	cd ../..

	# TODO: fix this hack!
	cp /boot/system/bin/libtool .
	ln -sfn $sourceDir/libtool /libtool

	runConfigure ./configure \
		--with-apr=$aprInstallDir \
		--with-expat=$expatInstallDir
	make $jobArgs
}

INSTALL()
{
	make install
	
	# remove libtool library file
	rm $libDir/libaprutil-1.la

	# prepare develop/lib
	prepareInstalledDevelLibs libaprutil-1
	fixPkgconfig

	# fix apu-1-config
	fixDevelopLibDirReferences $binDir/apu-1-config

	# remove superfluous .exp file
	rm $libDir/aprutil.exp
	
	# devel package
	packageEntries devel \
		$binDir \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	apr_util_devel = portVersion compat >= 1
	cmd:apu_1_config = $portVersion compat >= 1
	devel:libaprutil_1 = 0.5.4 compat >= 0
	"
REQUIRES_devel="
	apr_util == $portVersion base
	"
