SUMMARY="Cross-development tools for the Motorola 6809"
DESCRIPTION="LWTOOLS is a set of cross-development tools for the Motorola 6809
and Hitachi 6309 microprocessors. It supports assembling to raw binaries, CoCo
LOADM binaries, and a proprietary object file format for later linking. It also
supports macros and file inclusion among other things."
REVISION="1"
COPYRIGHT="2006-2014 William Astle"
LICENSE="GNU GPL v3"
HOMEPAGE="http://lwtools.projects.l-w.ca/"
ARCHITECTURES=""
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"
SRC_URI="$HOMEPAGE/releases/lwtools/lwtools-$portVersion.tar.gz"
CHECKSUM_SHA256="06ee27c597798bbfb064a8d9471e5cd91074ff22fa8eb4f7d276a8809d35622c"

PROVIDES="
	lwtools$secondaryArchSuffix = $portVersion
	cmd:lwar$secondaryArchSuffix
	cmd:lwasm$secondaryArchSuffix
	cmd:lwlink$secondaryArchSuffix
	cmd:lwobjdump$secondaryArchSuffix
	cmd:m6809_unknown_ar$secondaryArchSuffix
	cmd:m6809_unknown_as$secondaryArchSuffix
	cmd:m6809_unknown_ld$secondaryArchSuffix
	cmd:m6809_unknown_nm$secondaryArchSuffix
	cmd:m6809_unknown_objdump$secondaryArchSuffix
	cmd:m6809_unknown_ranlib$secondaryArchSuffix
	cmd:m6809_unknown_strip$secondaryArchSuffix
"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:cc$secondaryArchSuffix
	cmd:make
"

BUILD()
{
	make
}

INSTALL()
{
	make install INSTALLDIR=$binDir

	# Install the binutil-like interfaces to the tools
	cp extra/as $binDir/m6809-unknown-as
	cp extra/ar $binDir/m6809-unknown-ar
	cp extra/ld $binDir/m6809-unknown-ld

	ln -s /bin/true $binDir/m6809-unknown-nm
	ln -s /bin/true $binDir/m6809-unknown-objdump
	ln -s /bin/true $binDir/m6809-unknown-ranlib
	ln -s /bin/true $binDir/m6809-unknown-strip
}
