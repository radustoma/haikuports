SUMMARY="Checks and undeletes partitions + PhotoRec, signature based recovery tool"
DESCRIPTION="TestDisk is powerful free data recovery software\! \
It was primarily designed to help recover lost partitions and/or \
make non-booting disks bootable again when these symptoms are caused \
by faulty software: certain types of viruses or human error \
(such as accidentally deleting a Partition Table). \
Partition table recovery using TestDisk is really easy. \

TestDisk can: \

- Fix partition table, recover deleted partition
- Recover FAT32 boot sector from its backup
- Rebuild FAT12/FAT16/FAT32 boot sector
- Fix FAT tables
- Rebuild NTFS boot sector
- Recover NTFS boot sector from its backup
- Fix MFT using MFT mirror
- Locate ext2/ext3/ext4 Backup SuperBlock
- Undelete files from FAT, exFAT, NTFS and ext2 filesystem
- Copy files from deleted FAT, exFAT, NTFS and ext2/ext3/ext4 partitions.

TestDisk has features for both novices and experts. \
For those who know little or nothing about data recovery techniques, \
TestDisk can be used to collect detailed information about a \
non-booting drive which can then be sent to a tech for further analysis. \
Those more familiar with such procedures should find TestDisk a handy \
tool in performing onsite recovery.
"
HOMEPAGE="http://www.cgsecurity.org/wiki/TestDisk"
SRC_URI="git+http://git.cgsecurity.org/testdisk.git#c603941b7fd2323a4d8631ac05688b7b4460e088"
#CHECKSUM_SHA256=""
REVISION="1"
ARCHITECTURES="?x86 x86_gcc2 ?x86_64"
COPYRIGHT="1998-2008 Christophe GRENIER
	"

LICENSE="GNU GPL v2"

#TODO: separate package for qphotorec?

PROVIDES="
	testdisk = $portVersion
	cmd:testdisk = $portVersion
	cmd:photorec = $portVersion
	cmd:fidentify = $portVersion
	"

REQUIRES="
	haiku >= $haikuVersion
	lib:libiconv
	lib:libjpeg
	lib:libncurses
	lib:libuuid
	lib:libz
	"

BUILD_REQUIRES="
	haiku_devel >= $haikuVersion
	devel:libiconv
	devel:libjpeg
	devel:libncurses
	devel:libuuid
	devel:libz
	"

BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:make
	cmd:gcc
	cmd:pkg_config
	"

BUILD()
{
	#mkdir config
	#automake --add-missing -f -c
	autoreconf -vfi
	runConfigure ./configure
	make
}

INSTALL()
{
	make install
}
