SUMMARY="A viewer supporting PDF and DJVU Files"
DESCRIPTION="DocumentViewer is a viewer that supports PDF and DJVU Files."
LICENSE="MIT"
COPYRIGHT="2010-2014 Haiku, Inc"
HOMEPAGE="http://github.com/HaikuArchives/DocumentViewer/" 
SRC_URI="https://github.com/HaikuArchives/DocumentViewer/archive/eac703f.tar.gz"
REVISION="3"
SOURCE_DIR="DocumentViewer-eac703ff0c76c2bca660fc64eab2ec2739f331a4"
CHECKSUM_SHA256="8ab465d2d411d9a75d0d185f710a739b6b5749bf5b68a667222e315ad6879294"
ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	documentviewer$secondaryArchSuffix = $portVersion
	app:DocumentViewer = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libdjvulibre$secondaryArchSuffix
	lib:libfreetype$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	lib:libjpeg$secondaryArchSuffix
	lib:libopenjp2$secondaryArchSuffix
	lib:libjbig2dec$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libdjvulibre$secondaryArchSuffix
	devel:libmupdf$secondaryArchSuffix
	devel:libfreetype$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	devel:libjpeg$secondaryArchSuffix
	devel:libopenjp2$secondaryArchSuffix
	devel:libjbig2dec$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	cmd:find
	cmd:g++$secondaryArchSuffix
	cmd:jam
	"

BUILD()
{
	jam -q $jobArgs
}

INSTALL()
{
	mkdir -p $appsDir
	cp application/DocumentViewer $appsDir
	addAppDeskbarSymlink $appsDir/DocumentViewer
}
