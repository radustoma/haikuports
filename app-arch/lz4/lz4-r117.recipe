SUMMARY="lz4 data compression utilities and libraries"
DESCRIPTION="
LZ4 is a very fast lossless compression algorithm, providing compression speed \
at 400 MB/s per core, scalable with multi-cores CPU. It also features an \
extremely fast decoder, with speed in multiple GB/s per core, typically \
reaching RAM speed limits on multi-core systems.
"
HOMEPAGE="https://code.google.com/p/lz4/"
LICENSE="BSD (2-clause)"
COPYRIGHT="2011-2014, Yann Collet"
SRC_URI="https://github.com/Cyan4973/lz4/archive/r117.tar.gz"
SRC_FILENAME="$portVersionedName.tar.gz"
CHECKSUM_SHA256="c4ca70bf6711021d5ae64e79469a619a4e5899a4c7e07e665f3eb3a517cd029d"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86 x86_gcc2"

PATCHES="lz4-$portVersion.patchset"

PROVIDES="
	lz4${secondaryArchSuffix} = $portVersion
	lib:liblz4${secondaryArchSuffix} = 1.0.0 compat >= 1
	"

if [ -z "$secondaryArchSuffix" ]; then
PROVIDES="$PROVIDES
	cmd:lz4 = $portVersion
	cmd:lz4c = $portVersion
	cmd:lz4cat = $portVersion
	"
fi

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	"
BUILD_PREREQUIRES="
	cmd:ar
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	"

BUILD()
{
	make $jobArgs all
}

INSTALL()
{
	make install LIBDIR=$libDir INCLUDEDIR=$includeDir \
		BINDIR=$binDir MANDIR=$manDir 
	prepareInstalledDevelLibs liblz4
	
	packageEntries devel \
    	$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	lz4${secondaryArchSuffix}_devel = $portVersion
	devel:liblz4$secondaryArchSuffix = 1.0.0 compat >= 1
	"
REQUIRES_devel="
	lz4${secondaryArchSuffix} == $portVersion base
	"
